var user = null;
//{
//		nik : '',
//        name : '',
//        accountNo : '',
//        username : '',
//        password : '',
//        phone : '',
//        email : '',
//        address : ''	
//};

var argaApp = {
		name: 'UTS MP 1118100050',
		author: 'Arga Sudesta',
        client: 'Materi',
        error: '',
		contact: {
			email: '',
			phone: '',
			address: '',
			ig: '',
			tw: '',
			li: '',
			fb: '',
			yt: '',
			website: ''
		},
		footer: {
			year: '2020',
			name: 'Mobile Programming',
			website: '',
		},
		about: {
			description: '',
			vision: '',
			mission: ''
		},
		timeout: {
			connect: null,
			read: null
		},
		session: {
			loggedUser: null,
			deviceId: null,
			loginTime: null
		},
		alert:{
			system: {
				notFound: 'Page Not Found',
				internalError: 'Internal Error',
				underConstruction: 'Ooops... The page is under construction',
				invalidInput: 'You input invalid data',
				underMaintenance: 'System under maintenance, Come again later',
				differentVersion: 'Your device using deprecated version (Minimum Android 6 (Marshmellow))'
			},
			login: {
				success: 'Login Success',
				failed: 'Login Failed',
				invalidUsername: 'Invalid Username',
				invalidPassword: 'Invalid Password',
				emptyUsername: 'NIM Kosong',
				emptyPassword: 'Password Kosong',
			},
			common: {
				success: 'Success',
				fail: 'Failed',
				pending: 'On Progress',
				timeout: 'Timeout'
			}
		}
}

var dataInhouse = null;